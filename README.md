## \#SOLID, principios del diseño orientado a objetos en PHP 

**SOLID** es un acrónimo de los **cinco principios del diseño orientado a 
objetos** (*OOD Object Oriented Design*) creados por
**Robert C. Martin**

Los principios del diseño orientado a objetos SOLID proporcionan estabilidad y 
flexibilidad a las aplicaciones web en PHP

Los cinco principios son:

   + **S**. Single responsibility principle (*SRP*)
   + **O**. Open/Closed principle (*OCP*)
   + **L**. Liskov substitution principle (*LSP*)
   + **I**. Interface segregation principle (*ISP*)
   + **D**. Dependency Inversion Principle (*DIP*)
   
#### 1. Single responsibility principle (*SRP*)

***Una clase sólo debe tener un motivo para cambiar, 
lo que significa que sólo debe tener una tarea.***

+ Se aplica tanto a la clase como a cada uno de sus métodos
+ Este principio es quizás el más importante de todos, el más sencillo 
y a la vez el más complicado de llevar a cabo.

#### 2. Open/Closed principle (*OCP*)
***Los objetos o entidades deberían estar abiertas a su extensión, 
pero cerradas para su modificación.***

+ Este principio quiere decir que una clase debería ser fácilmente 
extendible sin modificar la propia clase.
+ Tenemos que ser capaces de extender el comportamiento de nuestras 
clases sin necesidad de modificar su código
+ El Open/Closed principle se suele resolver utilizando ***polimorfismo***


***En vez de obligar a la clase principal a saber cómo realizar una operación,
delega esta a los objetos que utiliza, de tal forma que no necesita saber 
explícitamente cómo llevarla a cabo. Estos objetos tendrán una interfaz 
común que implementarán de forma específica según sus requerimientos.***


#### 3. Liskov substitution principle (*LSP*)
***Si S es una subclase de T, entonces los objetos de T podrían ser 
substituidos por objetos del tipo S sin alterar las propiedades 
del problema. Esto es, cada clase que hereda de otra puede usarse 
como su padre sin necesidad de conocer las diferencias entre ellas.***

+ Cualquier subclase debería poder ser sustituible 
por la clase padre.
+ Cuando se hereda una clase no solo se heredan sus datos
sino también su comportamiento.
+ Si una función no cumple el **LSP** entonces rompe el **OCP**
+ El diseño por contrato (*Design by Contract*) es otra forma de llamar al **LSP**
+ Si los tests de la clase padre no funcionan para la hija, también estarás violando este principio


#### 4. Interface segregation principle (*ISP*)

***Una clase nunca debe ser forzada a implementar una interface que no usa, 
empleando métodos que no tiene por qué usar.***

+ Disminuye el acoplamiento.
+ Aumenta la Cohesión.
+ Genera un sistema más flexible.
+ Requiere tiempo y esfuerzo durante el diseño.

#### 5. Dependency inversion principle (*DIP*)

***A. Las clases de alto nivel no deberían depender de las clases 
de bajo nivel. Ambas deberían depender de las abstracciones.***

***B. Las abstracciones no deberían depender de los detalles. 
Los detalles deberían depender de las abstracciones.***

La inversión de dependencias da origen a la conocida **inyección de dependencias**, 
una de las mejores técnicas para lidiar con las colaboraciones entre clases, 
produciendo un código reutilizable, sobrio y preparado para cambiar.


---
fuentes:
+ [Librosweb]: https://librosweb.es/libro/tdd/capitulo_7/principios_solid.html
+ [devexperto]: https://devexperto.com/principio-responsabilidad-unica/
+ [devexperto]: https://devexperto.com/principio-responsabilidad-unica/
+ [scotch.io]: https://scotch.io/bar-talk/s-o-l-i-d-the-first-five-principles-of-object-oriented-design
