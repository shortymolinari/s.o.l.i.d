<?php

class Cube implements ShapeInterface, SolidShapeInterface
{
    public function area() {
        // Calcula la superficie del cubo
    }

    public function volume() {
        // Calcula el volumen del cubo
    }

    public function calculate() {
        return $this->area() + $this->volume();
    }
}