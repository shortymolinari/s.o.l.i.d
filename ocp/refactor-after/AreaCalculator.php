<?php
class AreaCalculator {

    protected $shapes;

    public function __construct($shapes = array()) {
        $this->shapes = $shapes;
    }

    public function sum() {
        foreach($this->shapes as $shape) {
            if($shape instanceof ShapeInterface) {
                $area[] = $shape->area();
                continue;
            }

            throw new AreaCalculatorInvalidShapeException;
        }

        return array_sum($area);
    }

    public function output() {
        return implode('', array(
            "<h1>",
                "Suma de todas las áreas: ",
                $this->sum(),
            "</h1>"
        ));
    }
}